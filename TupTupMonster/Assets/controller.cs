
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Specialized;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class controller : MonoBehaviour
{
    [SerializeField] private GameObject ButDestroy;
    public float timerdestroy;
    [SerializeField] private GameObject ButFreeze;
    public float timerfreeze;


    [SerializeField] private GameObject ColMonster;
    private string ColMonsterText;
    [SerializeField] private GameObject Lose;
    [SerializeField] private GameObject Score;
    [SerializeField] private GameObject ScoreLose;
    private string ScoreText;
    public float timermax;
    private float timer;
    private int rend;
    public int ColMonsters;
    public int n;
    public GameObject[] monsters;
    public int MaxMonster;
    private float RandX;
    private float RandY;
    public float score;
    public int up;
    private int upgrade;
    public float speed;
    private int i;
    public GameObject[] gameObjects;
    // Start is called before the first frame update
    void Start()
    {
        n = monsters.Length;
        timer = timermax;
    }

    // Update is called once per frame
    void Update()
    {
        if (ColMonsters >= MaxMonster)
        {
           // Lose.SetActive(true); ScoreText = "Score: " + score.ToString(); ScoreLose.GetComponent<Text>().text = ScoreText;
        }

    }

    void FixedUpdate()
    {
        if (ColMonsters < MaxMonster)
        {
            if (timer >= 0) { timer = timer - Time.deltaTime; }
            else { rend = Random.Range(0, n); RandX = Random.Range(-3.5f, 3.5f); RandY = Random.Range(8.5f, -8.8f); Instantiate(monsters[rend], new Vector3(RandX, RandY, 0), transform.rotation); ColMonsters++; timer = Random.Range(0f, timermax); upgrade++; if (upgrade == 10) { up++; upgrade = 0; } }
        }

        if (timerdestroy > 0) { timerdestroy = timerdestroy - Time.deltaTime; if (timerdestroy <= 0) { ButDestroy.SetActive(true); } }
        if (timerfreeze > 0) { timermax = 8f; timerfreeze = timerfreeze - Time.deltaTime; if (timerfreeze <= 0) { ButFreeze.SetActive(true); timermax = 3f; } }
    }

    void OnGUI()
    {

        ColMonsterText = "Monster: " + ColMonsters.ToString() + "/" + MaxMonster.ToString();
        ColMonster.GetComponent<Text>().text = ColMonsterText;
        ScoreText = "Score: " + score.ToString();
        Score.GetComponent<Text>().text = ScoreText;
    }

    public void PressDestroy()
    {
        timerdestroy = 15f;

        gameObjects = GameObject.FindGameObjectsWithTag("monsters");


        for (int k = 0; k < gameObjects.Length; ++k)
        {
            
                Destroy(gameObjects[k]); ColMonsters--;
        }
        ButDestroy.SetActive(false);
    }

    public void PressFreeze()
    {
        timerfreeze = 15f;

        
        ButFreeze.SetActive(false);
    }


    public void PressMenu()
    {
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);

    }
    public void PressRestart()
    {

        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }
    }
