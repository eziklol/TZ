using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Specialized;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class menu : MonoBehaviour
{
    [SerializeField] private GameObject RecordTable;
    [SerializeField] private GameObject TitlesTable;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void NewGame()
    {
        SceneManager.LoadScene("Game", LoadSceneMode.Single);

    }
    public void Records()
    {
        RecordTable.SetActive(true);
        

    }
    public void RecordsQuit()
    {
        RecordTable.SetActive(false);


    }
    public void Titles()
    {
        TitlesTable.SetActive(true);

    }
    public void TitlesQuit()
    {
        TitlesTable.SetActive(false);

    }
    public void Quit()
    {
        Application.Quit();

    }
}
